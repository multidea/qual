﻿using UnityEngine;
using System.Collections;
using Vuforia;
using UnityEngine.UI;

public class ButtonsScript : MonoBehaviour {

	public Button buttonMute;
	public Sprite SoundOff;
	public Sprite SoundOn;

	public Button buttonFlash;
	public Sprite FlashOff;
	public Sprite FlashOn; 

	public Button buttonInfo;
	public GameObject infoPanel;

	private Button buttonClose;

	private string MuteStatus = "SoundOn";
	private string FlashStatus = "FlashOff";
	private string InfoStatus = "InfoClose";


	// Use this for initialization
	void Start () {

		//Soud On
		buttonMute.image.overrideSprite = SoundOn;
		AudioListener.pause = false;
		AudioListener.volume = 1;
		MuteStatus = "SoundOn";

		// Flash Off
		buttonFlash.image.overrideSprite = FlashOff;
		CameraDevice.Instance.SetFlashTorchMode (false);
		FlashStatus = "FlashOff";
		infoPanel.SetActive(false);
	}


	// Button Mute
	public void muteChange () {
		Debug.Log ("Mute Toggle");

		if (MuteStatus == "SoundOn") {
			buttonMute.image.overrideSprite = SoundOff;
			AudioListener.pause = true;
			AudioListener.volume = 0;
			MuteStatus = "SoundOff";
		} else if (MuteStatus == "SoundOff") {
			buttonMute.image.overrideSprite = SoundOn;
			AudioListener.pause = false;
			AudioListener.volume = 1;
			MuteStatus = "SoundOn";
		}
	}

	// Button Flash
	public void FlashToggle () {
		Debug.Log ("Flash Toggle");
		
		if (FlashStatus == "FlashOn") {
			buttonFlash.image.overrideSprite = FlashOff;
			CameraDevice.Instance.SetFlashTorchMode (false);
			FlashStatus = "FlashOff";
		} else if (FlashStatus == "FlashOff") {
			buttonFlash.image.overrideSprite = FlashOn;
			CameraDevice.Instance.SetFlashTorchMode (true);
			FlashStatus = "FlashOn";
		}
	}

	// Button Info
	public void InfoToggle () {
		if (InfoStatus == "InfoClose") {
			Debug.Log ("Info Open");
			infoPanel.SetActive(true);
			InfoStatus = "InfoOpen";
		} else if (InfoStatus == "InfoOpen") {
			Debug.Log ("Info Close");
			infoPanel.SetActive(false);
			InfoStatus = "InfoClose";
		}
	}

	//Button Close
	public void Close () {
		if (Application.platform == RuntimePlatform.Android)
		{
			StopAllAudio();
			AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
			activity.Call<bool>("moveTaskToBack", true);				
		}
		else
		{
			StopAllAudio();
			Application.Quit();
		}
	}

	//Button Camera Inverse
	public void CameraInverse () {
		
	}
		
	//Stop all sounds
	private AudioSource[] allAudioSources;
	
	void StopAllAudio() {
		allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
		foreach( AudioSource audioS in allAudioSources) {
			audioS.Stop();
		}
	}

}

