using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Vuforia;

public class setFlash : MonoBehaviour {

	public Sprite OnIcon;
	public Sprite OffIcon;

	private Button buttonFlash;
	private bool Flash = false;

	void Start () {
		buttonFlash = GetComponent<Button>();
		FlashOff ();
	}

	public void FlashToggle (){
		if (Flash == false) {
			FlashOn ();
		} else if (Flash == true) {
			FlashOff ();
		}
	}

	private void FlashOn () {
		buttonFlash.image.overrideSprite = OnIcon;
		CameraDevice.Instance.SetFlashTorchMode (true);
		Flash = true;
	}

	private void FlashOff (){
		buttonFlash.image.overrideSprite = OffIcon;
		CameraDevice.Instance.SetFlashTorchMode (false);
		Flash = false;
	}
	
}



