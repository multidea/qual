using UnityEngine;
using System.Collections;
using Vuforia;
using UnityEngine.UI;

public class closeButton : MonoBehaviour {
	
	public void Close() {
		if (Application.platform == RuntimePlatform.Android)
		{
			StopAllSounds();
			AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
			activity.Call<bool>("moveTaskToBack", true);				
		}
		else
		{
			StopAllSounds();
			Application.Quit();
		}
	}

	//Parar todos os sons
	private AudioSource[] allAudioSources;

	void StopAllSounds() {
		allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
		foreach( AudioSource audioS in allAudioSources) {
			audioS.Stop();
		}
	}
}



