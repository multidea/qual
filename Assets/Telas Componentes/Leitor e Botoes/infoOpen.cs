﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class infoOpen : MonoBehaviour {

	public string InfoStatus;
	public GameObject infoPanel;
	public GameObject PassoPasso;

	void Start() {

	}

	// Button Info
	public void InfoToggle () {
		Debug.Log ("Toggle");

		if (!infoPanel.activeSelf) {
			infoPanel.SetActive(true);
			Debug.Log ("Info Aberto");
			Screen.orientation = ScreenOrientation.Portrait;
		} else {
			infoPanel.SetActive(false);
			Debug.Log ("Info Fechado");
			Screen.orientation = ScreenOrientation.LandscapeLeft;
		}
	}

	public void InfoClose(){
		infoPanel.SetActive(false);
		Debug.Log ("Info Fechado");
	}
}