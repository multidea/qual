using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Lean.Touch
{
	// This script allows you to transform the current GameObject
	public class SliderControler : MonoBehaviour
	{
		public RectTransform Container;

		public int SlidesQtd;
		public Image Bulets;
		public Sprite[] BuletsItens;
		public GameObject btnNext;
		public GameObject btnBack;

		public GameObject Leitor;
		public float slidActive;
		public float ajuste;
		public Vector2 screendeltaValues;

		private RectTransform SlidesPanel;
		public float size;
		public float slideSize;
		private float dest;
		private float left;

		public Vector2 distance;

		protected virtual void OnEnable()
		{
			// Hook into the events we need
			LeanTouch.OnFingerUp += OnFingerUp;
		}

		protected virtual void OnDisable()
		{
			// Unhook the events
			LeanTouch.OnFingerUp -= OnFingerUp;
		}


		void Start(){
			SlidesPanel = GetComponent<RectTransform>();

			float Width = Container.rect.width;
			SlidesPanel.offsetMax = new Vector2 ((Width * 3), SlidesPanel.offsetMax.y) ;

			slidActive = 1;
			size = SlidesPanel.offsetMax.x - SlidesPanel.offsetMin.x;
			slideSize = size / (SlidesQtd - 1);
		}

		public void OnFingerUp(LeanFinger finger)
		{
			Debug.Log("Finger " + finger.Index + " finished touching the screen");

			var fingers = LeanTouch.GetFingers(false, 0);
			var screenDistance = LeanGesture.GetScreenCenter(fingers);

			var limitY = Screen.height * 15 / 100;

			if (screenDistance.y >= limitY) {
				StartCoroutine (AutoPos(size, slideSize));
			}
		}

		public void nextSlide(){
			slidActive += 1;
			StartCoroutine (AutoPos(size, slideSize));
		}

		public void backSlide(){
			slidActive -= 1;
			StartCoroutine (AutoPos(size, slideSize));
		}

		public void ResetSliders(){
			slidActive = 1;
			StartCoroutine (AutoPos(size, slideSize));
		}

		protected virtual void Update()
		{
			var fingers = LeanTouch.GetFingers(false, 0);
			var screenDelta = LeanGesture.GetScreenDelta(fingers);
		
			Translate(screenDelta/2);
		} 

		private void Translate(Vector2 screenDelta)
		{

			float left = SlidesPanel.offsetMin.x;
			float rigt = SlidesPanel.offsetMax.x;
					
			float newRight = rigt += screenDelta.x;
			float newLeft = left += screenDelta.x;				

			if ((screenDelta.x > 0 && left > -130) || (screenDelta.x < 0 && rigt < 130)) {
				return;
			} else {
				SlidesPanel.offsetMax = new Vector2 (newRight , SlidesPanel.offsetMax.y);
				SlidesPanel.offsetMin = new Vector2 (newLeft, SlidesPanel.offsetMin.y);
			}

			float slidActiveDec = (-SlidesPanel.offsetMin.x + (slideSize / 2))/slideSize;
			slidActive = (int)slidActiveDec;
			Bulets.sprite = BuletsItens[(int)slidActive];
			}

		private IEnumerator AutoPos(float size, float slideSize){

			dest = -(slideSize*slidActive);

			left = SlidesPanel.offsetMin.x;
			float right = SlidesPanel.offsetMax.x;

			if (left > dest) {
				Debug.Log ("right");
				for (float lf = left; lf > dest; lf -= 2000f*Time.deltaTime) {				
					float DestLeft = lf;
					float DestRight = right - left + lf;

					SlidesPanel.offsetMax = new Vector2 (DestRight , SlidesPanel.offsetMax.y);
					SlidesPanel.offsetMin = new Vector2 (DestLeft, SlidesPanel.offsetMin.y);

					yield return new WaitForSeconds(0.01f);
				}
			}

			if (left < dest) {
				Debug.Log ("dir");
				for (float lf = left; lf <= dest; lf += 2500f*Time.deltaTime) {				
					float DestLeft = lf;
					float DestRight = right - left + lf;

					SlidesPanel.offsetMax = new Vector2 (DestRight , SlidesPanel.offsetMax.y);
					SlidesPanel.offsetMin = new Vector2 (DestLeft, SlidesPanel.offsetMin.y);

					yield return new WaitForSeconds(0.01f);
				}
			}

			SlidesPanel.offsetMax = new Vector2 (size + dest , SlidesPanel.offsetMax.y);
			SlidesPanel.offsetMin = new Vector2 (dest, SlidesPanel.offsetMin.y);

			if (slidActive < SlidesQtd - 1) {
				btnNext.SetActive (true);
			} else {
				btnNext.SetActive (false);
			}

			if (slidActive > 0) {
				btnBack.SetActive (true);
			} else {
				btnBack.SetActive (false);
			}

		}
	}
}