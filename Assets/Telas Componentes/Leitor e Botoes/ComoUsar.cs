﻿using UnityEngine;
using System.Collections;

public class ComoUsar : MonoBehaviour {

	public GameObject comoUsar;
	public GameObject ButtonsBar;
	private int UseVisualized;
	 
	// Use this for initialization
	void Start () {
		UseVisualized = PlayerPrefs.GetInt ("UseVisualized");
		Debug.Log ("ComoUsar: " + UseVisualized);

		if (UseVisualized == 0) {
			comoUsar.SetActive (true);
			ButtonsBar.SetActive(false);
			PlayerPrefs.SetInt ("UseVisualized",1);
		}
	}
	
	// Abrir Como Usar
	public void ComoUsarOpen () {
		comoUsar.SetActive(true);
		ButtonsBar.SetActive(false);
	}

	// Fechar Como Usar
	public void ComoUsarClose () {
		comoUsar.SetActive(false);
		ButtonsBar.SetActive(true);
	}

}
