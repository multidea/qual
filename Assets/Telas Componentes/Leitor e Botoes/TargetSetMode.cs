using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class TargetSetMode : MonoBehaviour,
ITrackableEventHandler
{
	private TrackableBehaviour mTrackableBehaviour;
	public GameObject TelaLeitor;
	//public GameObject TelaFound;

	void Start()
	{
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}
	
	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
		    newStatus == TrackableBehaviour.Status.TRACKED ||
		    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			//Modo Camera
			TelaLeitor.SetActive(false);
			//TelaFound.SetActive(true);
		}
		else
		{
			// Modo Leitor
			//TelaFound.SetActive(false);
			TelaLeitor.SetActive(true);
		}
	}   
}
