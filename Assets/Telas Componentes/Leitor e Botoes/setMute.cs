﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class setMute : MonoBehaviour {

	public Sprite OnIcon;
	public Sprite OffIcon;

	private Button buttonMute;
	private bool Mute = false;

	void Start () {
		buttonMute = GetComponent<Button>();
		MuteOff ();
	}

	public void MuteToggle (){
		if (Mute == false) {
			MuteOn ();
		} else if (Mute == true) {
			MuteOff ();
		}
	}

	private void MuteOn () {
		buttonMute.image.overrideSprite = OnIcon;
		AudioListener.pause = true;
		AudioListener.volume = 0;
		Mute = true;
	}

	private void MuteOff (){
		buttonMute.image.overrideSprite = OffIcon;
		AudioListener.pause = false;
		AudioListener.volume = 1;
		Mute = false;
	}
	
}



