﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaskedInputField : MonoBehaviour {
	public UnityEngine.UI.InputField inputField;
	public UnityEngine.UI.Text text;

	private string lastValidInput = string.Empty;
	private void Awake()
	{
		inputField.onValueChanged.AddListener( OnValueChanged );
	}
	private void OnValueChanged( string input ) // Supposing input = "12345678901"
	{
		string format = "({0}{1}){2}{3}{4}{5}-{6}{7}{8}{9}{10}" ;
		string[] array = new string[input.Length];
		int index = format.IndexOf("{" + (input.Length - 1) + "}");

		print (index);

		if ( input.Length == 0 )
		{
			text.text = string.Empty;
			return;
		}
		if ( index >= 0 )
		{
			int n = inputField.text.Length;

			if (n == 11) {
				format = "({0}{1}){2}{3}{4}{5}{6}-{7}{8}{9}{10}" ;
			}

			int toAdd = (n == 1) ? 3 : Mathf.CeilToInt(Mathf.Log10(n)) + 2;
			format = format.Substring (0, index + toAdd);
		}
		else
		{
			inputField.text = lastValidInput;
			return;
		}
		for ( index = 0 ; index < input.Length ; ++index )
			array[index] = string.Empty + input[index];
		lastValidInput = input;
		text.text = string.Format( format, array ); // Will output 12/34/5678/901
	}
}