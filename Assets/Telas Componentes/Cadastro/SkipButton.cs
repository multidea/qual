using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkipButton : MonoBehaviour {

	[SerializeField]
	private GameObject PainelCadastro = null;

	[SerializeField]
	private GameObject Sliders = null;

	[SerializeField]
	private GameObject TelaLeitor = null;

	public void DoSkip () {
		TelaLeitor.SetActive (true);
		StartCoroutine (fadeTransition ());
		PlayerPrefs.SetInt ("cadastroOK", 1);
	}

	IEnumerator fadeTransition(){
		for (float f=1f; f>=0f; f-=0.2f){
			PainelCadastro.GetComponent<CanvasGroup>().alpha = f;

			float minF = 1 - f;
			Sliders.GetComponent<CanvasGroup>().alpha = minF;
			yield return new WaitForSeconds (0.05f);				
		}
		PainelCadastro.SetActive (false);
	}

	IEnumerator fadeOutSliders(){
		for (float f=1f; f>=0f; f-=0.2f){
			Sliders.GetComponent<CanvasGroup>().alpha = f;
			yield return new WaitForSeconds (0.05f);				
		}
		PainelCadastro.SetActive (false);
		Sliders.SetActive (false);
		Sliders.GetComponent<CanvasGroup>().alpha = 1f;
	}

	void FadeOut(){
		StartCoroutine (fadeOutSliders ());
	}

	void Start() {
		Sliders.SetActive (false);
		PainelCadastro.SetActive (false);
		TelaLeitor.SetActive (true);

		if (PlayerPrefs.HasKey ("cadastroOK") == false) {
			TelaLeitor.SetActive (true);
			PainelCadastro.SetActive (true);
			Sliders.SetActive (true);
			Sliders.GetComponent<CanvasGroup>().alpha = 0f;
		}
	}
}
