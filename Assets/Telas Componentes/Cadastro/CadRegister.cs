﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CadRegister : MonoBehaviour {

	[SerializeField]
	private GameObject PainelCadastro = null;
	[SerializeField]
	private InputField NameField = null;
	[SerializeField]
	private InputField EmailField = null;
	[SerializeField]
	private InputField EstadoField = null;
    [SerializeField]
	private InputField CidadeField = null;
    [SerializeField]
	private Text Feedback = null;

	[SerializeField]
	private GameObject Sliders = null;

	[SerializeField]
	private string Url = null;

	[SerializeField]
	private GameObject TelaLeitor = null;

	[SerializeField]
	private bool ResetPrefs;


    private string selectedTime;

	public void EnviarDados(){

        //selectedTime = TimeField.options[TimeField.value].text;

		if (NameField.text != "" && EmailField.text != "" && EstadoField.text != "" && CidadeField.text != "") {
			string escapeName = WWW.EscapeURL (NameField.text);
			string escapeEmail = WWW.EscapeURL (EmailField.text);
			string escapeEstado = WWW.EscapeURL (EstadoField.text);
			string escapeCidade = WWW.EscapeURL (CidadeField.text);

			string UrlConcat = Url + "?name=" + escapeName + "&email=" + escapeEmail + "&estado=" + escapeEstado + "&cidade=" + escapeCidade;
			WWW webUrl = new WWW (UrlConcat);
			StartCoroutine (subEnviarDados (webUrl));
		} else {
			Feedback.text = "Por favor preencha os dados\ncorretamente para seguir adiante.";
			Feedback.canvasRenderer.SetAlpha (1f);
			Feedback.CrossFadeAlpha (0f, 3f, false);
		} 
	}

	IEnumerator subEnviarDados (WWW webUrl) {		
		yield return webUrl;

		if (webUrl.error == null) {
			string dataUrl = webUrl.text;
			TelaLeitor.SetActive (true);
			StartCoroutine (fadeTransition ());
			PlayerPrefs.SetInt ("cadastroOK", 1);
		} else {
			Feedback.text = "Erro ao enviar seus dados.\n Verifique sua conexão com a internet!";
			Feedback.canvasRenderer.SetAlpha (1f);
			Feedback.CrossFadeAlpha (0f, 3f, false);
			print (webUrl.error);
		}

	}

	IEnumerator fadeTransition(){
		for (float f=1f; f>=0f; f-=0.2f){
			PainelCadastro.GetComponent<CanvasGroup>().alpha = f;

			float minF = 1 - f;
			Sliders.GetComponent<CanvasGroup>().alpha = minF;
			yield return new WaitForSeconds (0.05f);				
		}
		PainelCadastro.SetActive (false);
	}

	IEnumerator fadeOutSliders(){
		for (float f=1f; f>=0f; f-=0.2f){
			Sliders.GetComponent<CanvasGroup>().alpha = f;
			yield return new WaitForSeconds (0.05f);				
		}
		PainelCadastro.SetActive (false);
		Sliders.SetActive (false);
		Sliders.GetComponent<CanvasGroup>().alpha = 1f;
		Screen.orientation = ScreenOrientation.LandscapeLeft;
	}

	public void FadeOut(){
		StartCoroutine (fadeOutSliders ());
	}

	void Start() {
		if (ResetPrefs) {
			PlayerPrefs.DeleteAll();
		}

		Sliders.SetActive (false);
		PainelCadastro.SetActive (false);
		TelaLeitor.SetActive (true);

		if (PlayerPrefs.HasKey ("cadastroOK") == false) {
			TelaLeitor.SetActive (true);
			PainelCadastro.SetActive (true);
			Sliders.SetActive (true);
			Sliders.GetComponent<CanvasGroup> ().alpha = 0f;
			Screen.orientation = ScreenOrientation.Portrait;
		} else {
			Screen.orientation = ScreenOrientation.LandscapeLeft;
		}

	}

	// Update is called once per frame
	void Update () {
		
	}
}
