﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropdownTImes : MonoBehaviour {

    private Dropdown dropTimes;

	// Use this for initialization
	void Start () {
        dropTimes = GetComponent<Dropdown>();
        listarTimes();
	}

    // Update is called once per frame
    void listarTimes() {
        List<string> Times = new List<string>()
        {
            "Time do coração",
            "América Mineiro",
            "Atlético Mineiro",
            "Atlético Paranaense",
            "Bahia",
            "Botafogo",
            "Ceará",
            "Chapecoense",
            "Cruzeiro",
            "Flamengo",
            "Fluminense",
            "Grêmio",
            "Internacional",
            "Palmeiras",
            "Paraná",
            "Santos",
            "Sport",
            "São Paulo",
            "Vasco da Gama",
            "Vitória",
            "Atlético Goianiense",
            "Avaí",
            "Boa Esporte",
            "Brasil de Pelotas",
            "Coritiba",
            "CRB",
            "Criciúma",
            "CSA",
            "Figueirense",
            "Fortaleza",
            "Goiás",
            "Guarani",
            "Juventude",
            "Londrina",
            "Oeste",
            "Paysandu",
            "Ponte Preta",
            "Sampaio Corrêa",
            "São Bento",
            "Vila Nova"

        };
        dropTimes.AddOptions(Times);
     }
}
