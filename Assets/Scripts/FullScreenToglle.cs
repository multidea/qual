﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class FullScreenToglle : MonoBehaviour {

	public VideoPlayer Player;
	public Camera TargetCamera;
	public Button FullButton;
	public Sprite FullIcon;
	public Sprite NormalIcon;
	public GameObject ControllBar;


	public void ToglleFullScreen (){
		if (Player.renderMode == VideoRenderMode.MaterialOverride) {
			SetFullScreen ();
			FullButton.image.overrideSprite = NormalIcon;
		} else {
			SetNormalScreen ();
			FullButton.image.overrideSprite = FullIcon;
		}

	}

	public void SetFullScreen() {
		Player.renderMode = VideoRenderMode.CameraNearPlane;
		Player.targetCamera = TargetCamera;
		Player.aspectRatio = VideoAspectRatio.FitOutside;
		ControllBar.SetActive (true);
	}

	public void SetNormalScreen() {
		Player.renderMode = VideoRenderMode.MaterialOverride;
		ControllBar.SetActive (false);
	}
}
