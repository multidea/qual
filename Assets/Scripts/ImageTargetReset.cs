using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class ImageTargetReset : MonoBehaviour,
ITrackableEventHandler 
{
	private TrackableBehaviour mTrackableBehaviour;
	public GameObject Model;

	void Start()
	{
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
			Debug.Log ("Reset Scale");
			Model.transform.localScale = new Vector3(1f, 1f, 1f);
			Model.transform.localRotation = new Quaternion (0f, 0f, 0f, 0f);
		}
	}
	
	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
		    newStatus == TrackableBehaviour.Status.TRACKED ||
		    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			Debug.Log ("Reset Scale");
			Model.transform.localScale = new Vector3(1f, 1f, 1f);
			Model.transform.localRotation = new Quaternion (0f, 0f, 0f, 0f);
		}
		else
		{
			Debug.Log ("Reset Scale");
			Model.transform.localScale = new Vector3(1f, 1f, 1f);
			Model.transform.localRotation = new Quaternion (0f, 0f, 0f, 0f);
		}
	}   
}
