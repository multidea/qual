﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using Vuforia;

public class YoutubePlay360 : MonoBehaviour,
ITrackableEventHandler{

	private TrackableBehaviour mTrackableBehaviour;
	public HighQualityPlayback VideoPlayback;
	public string VideoID;
	public Camera ARCarmera;
	public Camera Camera360;
	public GameObject Loadding;


	void Start()
	{
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}

	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			PlaybackPlay ();
		}
		else
		{
			PlaybackStop ();
		}
	}

	public void PlaybackPlay(){
		VideoPlayback.PlayYoutubeVideo(VideoID);
		ARCarmera.enabled = false;
		Camera360.enabled = true;
	}

	public void PlaybackStop(){		
		VideoPlayback.Stop ();
		ARCarmera.enabled = true;
		Camera360.enabled = false;
	}
}
