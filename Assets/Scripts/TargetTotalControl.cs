using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Vuforia;

public class TargetTotalControl : MonoBehaviour,
ITrackableEventHandler
{
	private TrackableBehaviour mTrackableBehaviour;
	private AudioSource[] allAudioSources;
	public AudioSource[] audios;

	[System.Serializable]
	public class AnimationControl {
		public Animator Controler;
		public string ClipName;
        public string IdleClipName;
    }
	public List<AnimationControl> Animations = new List<AnimationControl>();

	public ParticleSystem Particles;

	void Start()
	{
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
		StopParticles ();
	}


	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			StopAllSounds();	
			PlaySounds();
			PlayAnimations();
			PlayParticles ();
			StartCoroutine(ParticlesTime());
		}
		else
		{
			StopAllSounds();
			StopAnimations ();
			StopParticles ();
		}
	}

	// Controle de Som ------------------
	void StopAllSounds() {
		allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
		foreach( AudioSource audioS in allAudioSources) {
			PlayParticles ();
			audioS.Stop();
		}
	}

	void PlaySounds() {
		foreach( AudioSource audio in audios) {
			audio.Play ();
		}
	}

	//Controle de Animações ------------
	void PlayAnimations() {
		foreach( AnimationControl anin in Animations) {
			anin.Controler.Play (anin.ClipName, 0, 0f);
		}
	}

	void StopAnimations() {
		foreach( AnimationControl anin in Animations) {
			anin.Controler.Play (anin.IdleClipName, 0, 0f);
		}
	}

	//Controle de Particulas -----------
	IEnumerator ParticlesTime()
	{
		yield return new WaitForSeconds (1f);
		StopParticles ();
	}

	void PlayParticles()
	{
		if (Particles) {
			Particles.Play ();
			Particles.enableEmission = true;
		}
	}

	void StopParticles()
	{
		if (Particles) {
			Particles.enableEmission = false;
			Particles.Stop ();
		}
	} 
}