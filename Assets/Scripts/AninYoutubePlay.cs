﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AninYoutubePlay : MonoBehaviour {

	public HighQualityPlayback VideoPlayback;
	public string videoID;

	public void PlaybackPlay(){ 
		VideoPlayback.PlayYoutubeVideo(videoID);
	}

}
