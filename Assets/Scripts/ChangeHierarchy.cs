using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class ChangeHierarchy : MonoBehaviour,
ITrackableEventHandler
{
	private TrackableBehaviour mTrackableBehaviour;

	public GameObject ModelPai;
	public GameObject ModelFilho;
	public AninYoutubePlay AnimPlay;
	public string VideoID;


	void Start()
	{
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);

			// 
		}
	}

	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			ModelFilho.transform.parent = ModelPai.transform;
			ModelFilho.transform.localPosition = new Vector3 (0, -0.304f, 0);
			AnimPlay.videoID = VideoID;
		}
		else
		{
			//
		}
	}
}
