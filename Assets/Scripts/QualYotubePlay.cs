﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using Vuforia;

public class QualYotubePlay : MonoBehaviour,
ITrackableEventHandler{

	private TrackableBehaviour mTrackableBehaviour;
	public GameObject TelaVideo;
	public string VideoID;

	public GameObject ControllBar;

	public GameObject Children;
	public Vector3 DoublePagePosition;
	public Vector3 SinglePagePosition;

	public bool SinglePage;

	private HighQualityPlayback VideoPlayback;
	private FullScreenToglle FullScreen;

	void Start()
	{
		VideoPlayback = TelaVideo.GetComponent<HighQualityPlayback> ();
		FullScreen = TelaVideo.GetComponent<FullScreenToglle> ();

		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}

	}

	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			Children.transform.parent = transform;

			if (!SinglePage) {
				Children.transform.localPosition = DoublePagePosition;
			} else {
				Children.transform.localPosition = SinglePagePosition;
			}
			PlaybackPlay ();
		}
		else
		{
			PlaybackStop ();
		}
	}

	public void PlaybackPlay(){
		VideoPlayback.PlayYoutubeVideo(VideoID);
		//FullScreen.Player = TelaVideo.GetComponent<VideoPlayer>();
		FullScreen.SetNormalScreen ();
		TelaVideo.transform.localScale = new Vector3 (0, 0, 0);
	}

	public void PlaybackStop(){		
		VideoPlayback.Stop ();
		FullScreen.SetNormalScreen ();
		TelaVideo.transform.localScale = new Vector3 (0, 0, 0);
	}
}
