﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using System.IO;

public class CaptureScreen : MonoBehaviour {

	Texture2D tex;
	CaptureAndSave snapShot ;
	bool LeitorActive;

	public GameObject buttons; 
	public GameObject mask;
	public GameObject Leitor;
	public GameObject Preview;
	public RawImage PreviewImg;
	public GameObject MarcaDagua;
	private string screenShotPath;

	//public NativeShare ShareScript;

    
    private void Awake()
    {
        screenShotPath = Application.persistentDataPath + "/" + "screenshot.png";
    }

    void Start()
	{
		snapShot = GameObject.FindObjectOfType<CaptureAndSave>();
	}

	IEnumerator Capture(bool LeitorState)
	{
		yield return new WaitForSeconds(0.01f);

		if(tex != null)
		{	
			//Exibe a Mascara
			mask.SetActive (true);
			yield return new WaitForSeconds(0.01f);
			//Oculta a Mascara e Exibe Botoes
			mask.SetActive (false);
			buttons.SetActive (true);
			MarcaDagua.SetActive (false);
			Leitor.SetActive (LeitorState);
			yield return new WaitForSeconds(0.01f); 

			// save screenshot at Galery
			snapShot.SaveTextureAtPath(tex,screenShotPath,ImageType.PNG);
			snapShot.SaveTextureToGallery(tex,ImageType.JPG);

			//Carrega Preview
			PreviewImg.texture = tex;
			Preview.SetActive (true);
		}
	}

	void OnEnable()
	{
		CaptureAndSaveEventListener.onError += OnError;
		CaptureAndSaveEventListener.onSuccess += OnSuccess;
		CaptureAndSaveEventListener.onScreenShotInvoker += OnScreenShot;
	}

	void OnDisable()
	{
		CaptureAndSaveEventListener.onError -= OnError;
		CaptureAndSaveEventListener.onSuccess -= OnSuccess;
		CaptureAndSaveEventListener.onScreenShotInvoker -= OnScreenShot;
	}

	void OnError(string error)
	{
		Debug.Log ("Error : "+error);
	}

	void OnSuccess(string msg)
	{
		Debug.Log ("Success : "+msg);
	}

	void OnScreenShot(Texture2D tex2D)
	{
		// assign screenshot
		tex = tex2D;
		StartCoroutine(Capture(LeitorActive));
	}

	public void Capture()
	{
		buttons.SetActive (false);
		MarcaDagua.SetActive (true);

		if (Leitor.activeSelf) {
			Leitor.SetActive (false);
			LeitorActive = true;
		} else {
			LeitorActive = false;
		}

		snapShot.GetFullScreenShot(ImageType.JPG);
	}

	public void PreviewBack() {
		Preview.SetActive(false);
	}		

}
