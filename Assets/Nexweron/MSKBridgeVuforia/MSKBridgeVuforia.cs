﻿using UnityEngine;
using UnityEngine.Video;
using Nexweron.Core.MSK;
using Vuforia;
using System.Collections;
using Nexweron.WebCamPlayer;

public class MSKBridgeVuforia : MSKBridgeBase
{
	public MSKController mskController;

	private int _framesCounter = 0;

	private void Update() {
		if (_sourceTexture != null) {
			var framesCounter = VuforiaManager.Instance.CurrentFrameIndex;
			if (_framesCounter != framesCounter) {
				_framesCounter = framesCounter;

				mskController.RenderOut(_texture);
			}
		}
	}

	private void OnEnable() {
		VuforiaARController.Instance.RegisterBackgroundTextureChangedCallback(BackgroundTextureChanged);
		if (VuforiaRenderer.Instance.VideoBackgroundTexture != null) {
			BackgroundTextureChanged();
		}
	}
	private void OnDisable() {
		VuforiaARController.Instance.UnregisterBackgroundTextureChangedCallback(BackgroundTextureChanged);
	}
	
	private void BackgroundTextureChanged() {
		StartCoroutine(BackgroundTextureChangedEnumarator());
	}

	private IEnumerator BackgroundTextureChangedEnumarator() {
		yield return new WaitUntil(() => VuforiaRenderer.Instance.VideoBackgroundTexture.width > 0);

		UpdateTexture(VuforiaRenderer.Instance.VideoBackgroundTexture);

		var backgroundPlane = VuforiaManager.Instance.ARCameraTransform.GetComponentInChildren<BackgroundPlaneBehaviour>();
		var renderer = backgroundPlane.GetComponent<Renderer>();

		renderer.material.shader = Shader.Find("Unlit/Transparent");
		renderer.material.mainTexture = texture;
	}

	private void UpdateTexture(Texture texture) {
		if (sourceTexture != texture) {
			sourceTexture = texture;

			if (mskController != null) {
				mskController.SetSourceTexture(sourceTexture);
			} else {
				Debug.LogError("MSKBridgeVuforia | mskController = null");
			}
		}
	}
}
