﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowTela : MonoBehaviour {

	public GameObject TelaVideo;

	public void ExibirTela(){
		TelaVideo.GetComponent<MeshRenderer> ().enabled = true;
		TelaVideo.transform.localScale = new Vector3 (0.038f, 0.4f, 0.024f);
	}
}
